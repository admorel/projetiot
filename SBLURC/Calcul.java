package SBLURC;

import java.util.ArrayList;

public class Calcul {

    public int tireNombreAleatoire(int rangeOfNumber){
        return (int)(Math.random()*rangeOfNumber)+1;
    }

    public String tireOpérateur(){
        String[] operateur = new String[]{"+", "-", "/", "*"};

        return operateur[(int)(Math.random()*operateur.length)];
    }

    public ArrayList<String> genereListOperateur(int nbOperateur){
        ArrayList<String> operateurList = new ArrayList<>();

        for(int i = 0; i < nbOperateur; i++){
            String op = tireOpérateur();
            if(operateurList.contains("-") || operateurList.contains("/")){
                while(op == "-" || op == "/"){
                    op = tireOpérateur();
                }
            }
            operateurList.add(op);
        }

        return operateurList;
    }



    private String generateOperation(ArrayList<Integer> nbToCalculate, ArrayList<String> operateur){
        String operation = "";
        if(operateur.size() == 1){
            switch (operateur.get(0)){
                case "+":
                    operation = nbToCalculate.get(0).toString() + operateur.get(0) + nbToCalculate.get(1);
                    break;

                case "*":
                    operation = nbToCalculate.get(0).toString() + operateur.get(0) + nbToCalculate.get(1);
                    break;


                case "/":
                    if(nbToCalculate.get(0) < nbToCalculate.get(1)){
                        operation = nbToCalculate.get(1).toString() + operateur.get(0) + nbToCalculate.get(0);
                    }
                    else{
                        operation = nbToCalculate.get(0).toString() + operateur.get(0) + nbToCalculate.get(1);
                    }
                    break;

                default:
                    break;
            }
        }
        if(operateur.size() == 2){
            switch (operateur.get(0)){
                case "/":
                    if(nbToCalculate.get(0) < nbToCalculate.get(1)){
                        operation = "(" + nbToCalculate.get(1).toString() + operateur.get(1) + nbToCalculate.get(0) + ")" + operateur.get(0) + nbToCalculate.get(2);
                    }
                    else{
                        operation = "(" + nbToCalculate.get(0).toString() + operateur.get(1) + nbToCalculate.get(1) + ")" +operateur.get(0) + nbToCalculate.get(2);
                    }
                    break;

                default:
                    if(operateur.get(1) == "/"){
                        operation = "(" + nbToCalculate.get(0).toString() + operateur.get(0) + nbToCalculate.get(1) + ")" + operateur.get(1) + nbToCalculate.get(2);
                    }
                    else{
                        operation = nbToCalculate.get(0).toString() + operateur.get(0) + nbToCalculate.get(1) + operateur.get(1) + nbToCalculate.get(2);
                    }
                    break;


            }
        }

        return operation;
    }





    public String genereCalcul(int niveauUtilisateur){

        ArrayList<Integer> nbToCalculate = new ArrayList<>();
        ArrayList<String> operateur = new ArrayList<>();

        String operation = "";


        if(niveauUtilisateur < 50) //Facile
        {

            for (int i = 0; i < 2; i++){
                nbToCalculate.add(tireNombreAleatoire(10));
            }

            operateur = genereListOperateur(1);

            operation = generateOperation(nbToCalculate, operateur);


        }



        if(50 <= niveauUtilisateur && niveauUtilisateur < 150) //Moyen
        {

            for (int i = 0; i < 3; i++){
                nbToCalculate.add(tireNombreAleatoire(10));
            }

            operateur = genereListOperateur(2);

            operation = generateOperation(nbToCalculate, operateur);

        }


        if(150 <= niveauUtilisateur) //Difficile
        {

            for (int i = 0; i < 3; i++) {
                nbToCalculate.add(tireNombreAleatoire(100));
            }

            operateur = genereListOperateur(2);

            operation = generateOperation(nbToCalculate, operateur);

        }

        return operation;

    }

}
